package giorgos.demosuite.junit.info;
import static org.junit.Assert.assertTrue;
import java.util.Map;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import giorgos.demosuite.testbase.TestBase;
import io.restassured.RestAssured;
import io.restassured.config.RedirectConfig;
import io.restassured.response.Response;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Title;


@RunWith(SerenityRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ScraperLoginTests extends TestBase {
	
	
	
	 
	
	@Title("Logging in with correct username and password - passing cookies and handling redirection")
	@Test
	public void test001()
	{
		
		//sending POST and collecting cookies 
		Map<String, String> myCookies = SerenityRest.rest().given().config(RestAssured.config().redirect(RedirectConfig.redirectConfig().followRedirects(true).and().maxRedirects(5)))
				.header("Content-Type","multipart/form-data")
				.queryParam("mode", "login")
				.multiPart("usr", "admin")
				.multiPart("pwd", "12345")
				.when()
				.post()
				.then()
				.statusCode(302)
				.extract()
				.response()
				.getCookies();
				
		
		System.out.println("my cookies are: "+myCookies);
	
		
		//faking our cookies into the new get  
		 Response response = SerenityRest.rest().given()
		.contentType("multipart/form-data")
		.cookies(myCookies)
		.get("http://testing-ground.scraping.pro/login?mode=welcome");
		 
		 //exporting response body for validation 
		String body = response.getBody().asString();
		

	
		
		System.out.println("--------------");
		System.out.println(body);
	
//		//validation
		assertTrue("Access granted verified!", body.contains("<h3 class='success'>WELCOME :)</h3>"));
		
	
		
		
	}
	
	@Title("going back to login page - emulating the <<GO BACK link")
	@Test
	public void test002()
	{
		String body = SerenityRest.rest().given()
				.when()
				.get("http://testing-ground.scraping.pro/login")
				.then()
				.log()
				.all()
				.statusCode(200)
				.extract()
				.asString();
		
		//validation
		assertTrue("Return to login screen verified!", body.contains("<h3>Please, login:</h3>"));
		
		
	}
	
	
	
	@Title("Attempting to login with incorrect username and password")
	@Test
	public void test003()
	{
		
		//sending POST with incorrect username and password 
		String response = SerenityRest.rest().given().config(RestAssured.config().redirect(RedirectConfig.redirectConfig().followRedirects(true).and().maxRedirects(5)))
				.header("Content-Type","multipart/form-data")
				.queryParam("mode", "login")
				.multiPart("usr", "giorgos")
				.multiPart("pwd", "myPass")
				.when()
				.post()
				.then()
				.log()
				.all()
				.statusCode(200)
				.extract()
				.asString();
	
		
	
		//validation
		assertTrue("Access denied verified!", response.contains("<h3 class='error'>ACCESS DENIED!</h3>"));
		
	
		
		
	}
	
	

	@Title("Logging in but not handling redirection")
	@Test
	public void test004()
	{
		
		String response = SerenityRest.rest().given().config(RestAssured.config().redirect(RedirectConfig.redirectConfig().followRedirects(true).and().maxRedirects(5)))
				.header("Content-Type","multipart/form-data")
				.queryParam("mode", "login")
				.multiPart("usr", "admin")
				.multiPart("pwd", "12345")
				.when()
				.post()
				.then()
				.statusCode(302)
				.extract()
				.asString();
			
	
		//validation
		assertTrue("Stuck in redirection verified!", response.contains("<h3 class='success'>REDIRECTING...</h3>"));
			
		
	}
	
	
	
	
	@Title("Logging in while handling redirection but with wrong cookie")
	@Test
	public void test005()
	{
		//optional step - acquiring cookies	
		Map<String, String> myCookies = SerenityRest.rest().given().config(RestAssured.config().redirect(RedirectConfig.redirectConfig().followRedirects(true).and().maxRedirects(5)))
				.header("Content-Type","multipart/form-data")
				.queryParam("mode", "login")
				.multiPart("usr", "admin")
				.multiPart("pwd", "12345")
				.when()
				.post()
				.then()
				.extract()
				.response()
				.getCookies();
				
		//corrupting cookies
		String corruptedCookies = myCookies+"wrong cookies";
	
		
		//faking our cookies into the new get  
		 Response response = SerenityRest.rest().given()
		.contentType("multipart/form-data")
		.cookie(corruptedCookies)
		.get("http://testing-ground.scraping.pro/login?mode=welcome");
		 
		 //exporting response body for validation 
		String body = response.getBody().asString();
		
	
		//validation
		assertTrue("Wrong cookies verified!", body.contains("<h3 class='error'>THE SESSION COOKIE IS MISSING OR HAS A WRONG VALUE!</h3>"));
		
	
		
		
	}
	
}
