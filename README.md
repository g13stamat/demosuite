This demo test suite is build as a java maven project using Serenity and RestAssured.

In order to build it

1. unzip the folder and import it to eclipse (or similar IDE)  as a Maven project.
2. in package explorer right-click on the DemoSuite -> Maven -> update project -> select DemoSuite from the list and click OK

Maven will take care of the dependencies for you. The actual tests are located in the `ScraperLoginTests.java` file.

You can execute the tests by right clicking the file in the `package explorer -> Run as -> JUnit test`
or you can create a new maven build passing as goals: `clean verify serenity:aggregate` and save it.

Then right-click the `pom.xml -> Run As -> <your Maven configuration>`
the serenity html report for the mini suite is located in: `/DemoSuite/target/site/serenity/index.html`
 